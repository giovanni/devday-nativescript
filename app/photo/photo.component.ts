import { Component, OnInit } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http'
import * as camera from "nativescript-camera";
import * as bghttp from "nativescript-background-http"
import { Image } from "ui/image";
import { ImageAsset } from "image-asset"
import * as imageSourceModule from "image-source";

var session = bghttp.session("image-upload");

import { Observable } from "rxjs/Observable"
import "rxjs/add/observable/fromPromise"
import "rxjs/add/observable/empty"
import "rxjs/add/operator/map"
import "rxjs/add/operator/catch"

@Component({
    moduleId: module.id,
    selector: 'photo-component',
    templateUrl: 'photo.component.html'
})
export class PhotoComponent implements OnInit {
    imageSrc: any = ""
    event: string = ""
    constructor(private http: Http) { }

    ngOnInit() {
        camera.requestPermissions()
    }

    takePhoto() {
        console.info("Take photo")
        let options = { width: 1024, height: 1024, keepAspectRatio: false, saveToGallery: true };
        camera.takePicture(options).then((imageAsset: ImageAsset) => {
            console.log("Result is an image asset instance");
            this.imageSrc = imageAsset
        }).catch(function (err) {
            console.log("Error -> " + err.message);
        });
    }

    sendPhoto() {
        this.base64().subscribe(base => {
            console.info(base)
            let headers = new Headers()
            headers.append("Authorization", "Client-ID 62c6fed1d77f43d")
            let options = new RequestOptions()
            options.headers = headers
            this.http.post('https://api.imgur.com/3/image', {
                'image': "data:image/png;base64," + base,
                'type': 'base64'
            }, options)
            .catch((error, caught) => {
                console.error(error)
                return Observable.empty()
            })
            .subscribe((response: any) => {
                console.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>>")
                console.info(response)
                console.info(response.data.link)
            })
        })

    }
    base64(): Observable<string> {

        let promise = imageSourceModule.fromAsset(this.imageSrc);
        
        return Observable.fromPromise(promise).map(value => {
            let base64 = value.toBase64String("PNG")
            return base64
        })

    }

}